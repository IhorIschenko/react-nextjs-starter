# React nextjs starter kit

## This is a React Next.js starter kit and templates.

### Start localy

> npm run dev

### Build the project

> npm run build

### Start .next folder build

> npm run start

### Lint and format your code

> npm run lint

> npm run format

### Main stack

-   Next.js
-   TypeScript
-   Sass

### Env files

-   .env
-   .env.local
-   .env.staging (not done yet)
-   .env.qa (not done yet)
-   .env.production (not done yet)

### Linting code

-   .eslintrc.js and .eslintignore
-   .prettierrc and .prettierignore
-   linting and formatting code on commit (not done yet)

### Config files and docker

-   next.config.js
-   tsconfig.json
-   webpack.config.ts
-   .gitignore
-   Dockerfile
-   .dockerignore
