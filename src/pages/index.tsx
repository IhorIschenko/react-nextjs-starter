import { ReactElement } from 'react';

import getPosts from '@api/home.api';

import styles from '@styles/home.module.scss';

function Home({ posts }: any): ReactElement {
    return <div className={styles.title}>Hello there! it is homepage</div>;
}

export async function getStaticProps(): Promise<{
    props: any;
    revalidate: number;
}> {
    const posts = await getPosts();

    return {
        props: { posts },
        revalidate: 1
    };
}

export default Home;
