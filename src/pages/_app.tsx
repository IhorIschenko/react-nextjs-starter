import { ReactElement } from 'react';

import '@styles/global.styles.scss';

export default function MyApp({
    Component,
    pageProps
}: {
    Component: React.FC;
    pageProps: any;
}): ReactElement {
    return <Component {...pageProps} />;
}
