import axios from 'axios';

import IPosts from '@interfaces/home.interface';

export default function getPosts(): Promise<IPosts[]> {
    return axios.get('https://api.tvmaze.com/search/shows?q=sherlock').then(
        (response) => {
            return response.data;
        },
        (error) => {
            console.log(error);
            return error;
        }
    );
}
